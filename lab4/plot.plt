set term png
set y2tic nomirror
set yrange []
set xrange []
set xzeroaxis

set out "1a50.png"
p "1_50.dat" u 1:2 w l t "Stream numeric", \
"" u 1:3 w l t "Stream real",\
"" u 1:4 w l t "Vorticity numeric",\
"" u 1:5 w l t "Vorticity real"

set out "1a250.png"
p "1_250.dat" u 1:2 w l t "Stream numeric", \
"" u 1:3 w l t "Stream real",\
"" u 1:4 w l t "Vorticity numeric",\
"" u 1:5 w l t "Vorticity real"


set out "1_u.png"
p "1_u.dat" u 1:2 w l t "u(y) numeric",\
"" u 1:3 w l t "u(y) real"