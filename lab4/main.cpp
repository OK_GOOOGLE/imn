#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#define PI 3.14159265358979323846
const double tol = 1e-7;
const double dxy = 0.01;

inline double sqr(const double& a){
	return a*a;
}

inline double cube(const double& a){
	return a*a*a;
}

class Point{
public:
	int x;
	int y;

	Point(int newX, int newY): x(newX), y(newY) {}
};

class Hidrance{
public:
	Point A;
	Point B;
	Point C;
	Point D;

	Hidrance(Point newA, Point newB, Point newC, Point newD): A(newA), B(newB),
															  C(newC), D(newD) {}
};	


bool isHidrance(int x, int y, Hidrance& hidrance){
		if (x > hidrance.A.x && y > hidrance.B.y && x < hidrance.B.x) { return true; }
		if (x > hidrance.C.x && x < hidrance.D.x && y > hidrance.D.y)  { return true; }
		return false;
}
double vorticityReal(int y, double Q = -1){
	return Q * 0.5 * (2*y*dxy - 0.9); // mi = 1
}
double streamReal(int y, double Q = -1){
	return Q * 0.5 * (cube(y*dxy) / 3.0 - sqr(y*dxy) * 0.5 * 0.9); // ymin =0
}

void initStream(double array[301][91], double Q = -1){
	int i, j;
	for (i = 0; i <= 300; ++i){
		array[i][0]  = streamReal(0, Q);
		array[i][90] = streamReal(90, Q);
	}
	for (j = 0; j <= 90; ++j)
		array[0][j] = array[300][j]  = streamReal(j, Q);	

	for (i = 1; i < 300; ++i)
		for (j = 1; j < 90; ++j)
			array[i][j] = 0.0;
}
void initVorticity(double array[301][91], double Q = -1){
	int i, j;
	for (i = 0; i <= 300; ++i){
		array[i][0]  = vorticityReal(0, Q);
		array[i][90] = vorticityReal(90, Q);
	}
	for (j = 0; j <= 90; ++j)
		array[0][j] = array[300][j]  = vorticityReal(j, Q);	

	for (i = 1; i < 300; ++i)
		for (j = 1; j < 90; ++j)
			array[i][j] = 0.0;
}
void calcStream(double stream[301][91], double vorticity[301][91]){
	for (int i = 1; i < 300; ++i)
		for (int j = 1; j < 90; ++j)
			stream[i][j] = (stream[i-1][j] + 
						    stream[i+1][j] + 
						    stream[i][j-1] + 
						    stream[i][j+1] -
						    vorticity[i][j] * 0.0001) / 4.0;
}
void calcVorticity(double stream[301][91], double vorticity[301][91]){
	for (int i = 1; i < 300; ++i)
		for (int j = 1; j < 90; ++j)
			vorticity[i][j] = (vorticity[i-1][j] + 
						       vorticity[i+1][j] + 
						       vorticity[i][j-1] + 
						       vorticity[i][j+1]) / 4.0  - 
						       (1/16.0) * (
						       		(stream[i][j+1] - stream[i][j-1]) * (vorticity[i+1][j] - vorticity[i-1][j]) - 
						       		(stream[i+1][j] - stream[i-1][j]) * (vorticity[i][j+1] - vorticity[i][j-1])	
						       			);
}

void zad1(){
	int x, y;
	double stream[301][91];
	double vorticity[301][91];
	FILE* fp1;
	FILE *fp2;
	fp1 = fopen("1_50.dat", "w");
	fp2 = fopen("1_250.dat", "w");

	initStream(stream);
	initVorticity(vorticity);
	
	// initArray(array);
	double streampre = 10, vortpre = 10;
	int it = 0;
	while(fabs(streampre - stream[145][45])  > tol ||
		  fabs(vortpre - vorticity[145][45]) > tol ||   it < 150){
		streampre = stream[145][45];
		vortpre   = vorticity[145][45];
		calcStream(stream, vorticity);
		calcVorticity(stream, vorticity);
		++it;

	}
	for (int j = 0; j <= 90; ++j){
		/*50 */fprintf(fp1, "%.2f \t %f \t %f \t %f \t %f \n", dxy*j, stream[50][j], streamReal(j), vorticity[50][j], vorticityReal(j));
		/*250*/fprintf(fp2, "%.2f \t %f \t %f \t %f \t %f \n", dxy*j, stream[250][j], streamReal(j), vorticity[250][j], vorticityReal(j));
	}
	

	fclose(fp1);
	fclose(fp2);
	fp1 = fopen("1_u.dat", "w");
	double u, ureal;
	for (int j = 0; j < 90; ++j){
		u = (stream[50][j+1] - stream[50][j]) / dxy;
		ureal = -0.5 * j*dxy * (j*dxy - 0.9);
		fprintf(fp1, "%f \t %f \t %f \n", j*dxy, u, ureal);
	}
	fclose(fp1);
}

void initStreamHidrance(double vorticity[301][91], double stream[301][91], Hidrance& hidrance, double Q = -1){
	int i,j;
	/* dolny i gorny brzegi */
	for(i = 0; i <= 300; ++i){
		stream[i][0] = streamReal(0, Q); 
		if (!isHidrance(i, 90, hidrance)) stream[i][90] = streamReal(90, Q);
	}

	/* lewy i prawy brzegi */
	for(j = 0; j <= 90; ++j){
		stream[0][j] = streamReal(j, Q); 
		stream[300][j] = streamReal(j, Q);
	}
	/* obrys */
	/*-A*/
	for (j = hidrance.A.y; j <= 90; ++j){
		stream[hidrance.A.x][j] = stream[hidrance.A.x+1][j] = streamReal(90, Q);
	}
	/*AB*/
	for (i = hidrance.A.x; i <= hidrance.B.x; ++i){
		stream[i][hidrance.A.y] = streamReal(90, Q);
	}
	/*BC*/
	for (j = hidrance.C.y; j <= hidrance.B.y; ++j){
		stream[hidrance.B.x][j] = stream[hidrance.B.x + 1][j]= streamReal(90, Q);
	}
	/*CD*/
	for (i = hidrance.C.x; i <= hidrance.D.x; ++i){
		stream[i][hidrance.C.y] = streamReal(90, Q);
	}
	/*D-*/
	for (j = hidrance.D.y; j <= 90; ++j){
		stream[hidrance.D.x][j] = stream[hidrance.D.x - 1][j] = streamReal(90, Q);
	}
}

void initVorticityHidrance(double vorticity[301][91], double stream[301][91], Hidrance& hidrance, double Q = -1){
	int i, j;
	/*dolny i gorny brzegi*/
	for(i = 0; i <= 300; ++i){
		vorticity[i][0] = 2 * (stream[i][0+1] - stream[i][0]) / 0.0001; 
		if (!isHidrance(i, 90, hidrance)) 
			vorticity[i][90] = 2 * (stream[i][90-1] - stream[i][90]) / 0.0001;
	}

	/* lewy i prawy brzegi */
	for(j = 0; j <= 90; ++j){
		vorticity[0][j] = vorticityReal(j, Q); 
		vorticity[300][j] = vorticityReal(j, Q);
	}

	/*odcinek "B" = -A*/
	for (j = hidrance.A.y; j <= 90; ++j){
		vorticity[hidrance.A.x][j] = 2 * (stream[hidrance.A.x-1][j] - stream[hidrance.A.x][j]) / 0.0001;
	}
	/*odcinek "D" = BC*/
	for (j = hidrance.C.y; j <= hidrance.B.y; ++j){
		vorticity[hidrance.B.x][j] = 2 * (stream[hidrance.B.x-1][j] - stream[hidrance.B.x][j]) / 0.0001;
	}
	/*odcinek "F" = D-*/
	for (j = hidrance.D.y; j <= 90; ++j){
		vorticity[hidrance.D.x][j] = 2 * (stream[hidrance.D.x+1][j] - stream[hidrance.D.x][j]) / 0.0001;
	}

	/*odcinek "C" = AB*/
	for(i = hidrance.A.x; i <= hidrance.B.x; ++i){
		vorticity[i][hidrance.A.y] = 2 * (stream[i][hidrance.A.y-1] - stream[i][hidrance.A.y]) / 0.0001;
	}
	/*odcinek "E" = CD*/
	for(i = hidrance.C.x; i <= hidrance.D.x; ++i){
		vorticity[i][hidrance.C.y] = 2 * (stream[i][hidrance.C.y-1] - stream[i][hidrance.C.y]) / 0.0001;
	}

	/*naroznik A */
	double left = 2 * (stream[hidrance.A.x-1][hidrance.A.y] - stream[hidrance.A.x][hidrance.A.y]) / 0.0001;
	double right  = 2 * (stream[hidrance.A.x][hidrance.A.y-1] - stream[hidrance.A.x][hidrance.A.y]) / 0.0001;
	vorticity[hidrance.A.x][hidrance.A.y] = (right + left) / 2.0;
	/*naroznik C */
	left  = 2 * (stream[hidrance.C.x-1][hidrance.C.y] - stream[hidrance.C.x][hidrance.C.y]) / 0.0001;
	right = 2 * (stream[hidrance.C.x][hidrance.C.y-1] - stream[hidrance.C.x][hidrance.C.y]) / 0.0001;
	vorticity[hidrance.C.x][hidrance.C.y] = (right + left) / 2.0;
	/*naroznik D */
	left  = 2 * (stream[hidrance.D.x][hidrance.D.y-1] - stream[hidrance.D.x][hidrance.D.y]) / 0.0001; // odcinek "E"
	right = 2 * (stream[hidrance.D.x+1][hidrance.D.y] - stream[hidrance.D.x][hidrance.D.y]) / 0.0001;
	vorticity[hidrance.D.x][hidrance.D.y] = (right + left) / 2.0;


}



void calcStream2(double stream[301][91], double vorticity[301][91], Hidrance& hidrance){
	for (int i = 1; i < 300; ++i)
		for (int j = 1; j < 90; ++j)
			if (!isHidrance(i-1, j, hidrance) && 
				!isHidrance(i+1, j, hidrance) && 
				!isHidrance(i, j+1, hidrance) &&
				!isHidrance(i, j-1, hidrance) &&
				!isHidrance(i, j, hidrance))
			stream[i][j] = (stream[i-1][j] + 
						    stream[i+1][j] + 
						    stream[i][j-1] + 
						    stream[i][j+1] -
						    vorticity[i][j] * 0.0001) / 4.0;
}
void calcVorticity2(double stream[301][91], double vorticity[301][91], Hidrance& hidrance){
	for (int i = 1; i < 300; ++i)
		for (int j = 1; j < 90; ++j)
			if (!isHidrance(i-1, j, hidrance) && 
				!isHidrance(i+1, j, hidrance) && 
				!isHidrance(i, j+1, hidrance) &&
				!isHidrance(i, j-1, hidrance) &&
				!isHidrance(i, j, hidrance))
			vorticity[i][j] = (vorticity[i-1][j] + 
						       vorticity[i+1][j] + 
						       vorticity[i][j-1] + 
						       vorticity[i][j+1]) / 4.0  - 
						       (1/16.0) * (
						       		(stream[i][j+1] - stream[i][j-1]) * (vorticity[i+1][j] - vorticity[i-1][j]) - 
						       		(stream[i+1][j] - stream[i-1][j]) * (vorticity[i][j+1] - vorticity[i][j-1])	
						       			);
}

void zad2_1(){
	int x, y;
	double stream[301][91];
	double vorticity[301][91];
	FILE* fp1;
	FILE *fp2;
	fp1 = fopen("2_1psi.dat", "w");
	fp2 = fopen("2_1u.dat", "w");
	Hidrance hidrance(Point(85, 70), Point(101, 70), Point(101, 50), Point(116, 50));

	initStream(stream);
	initVorticity(vorticity);

	double streampre = 10, vortpre = 10;
	int it = 0;
	while(fabs(streampre - stream[145][45])  > tol ||
		  fabs(vortpre - vorticity[145][45]) > tol ||   it < 150){
		++it;
		initStreamHidrance(vorticity, stream, hidrance);
		initVorticityHidrance(vorticity, stream, hidrance);
		streampre = stream[145][45];
		vortpre   = vorticity[145][45];
		calcStream2(stream, vorticity, hidrance);
		calcVorticity2(stream, vorticity, hidrance);
		// printf("%f %f \n", fabs(streampre - stream[145][45]), fabs(vortpre - vorticity[145][45]));
	}
	for(int x = 1; x < 301; x++){
    	for(int y = 1; y < 91; y++){
			fprintf(fp1, "%f \t %f \t %f\n", x*dxy, y*dxy, stream[x][y]);
      	}
      	fprintf(fp1, "\n");
    }

    for(int x = 1; x < 301; x++){
    	for(int y = 1; y < 91; y++){
			fprintf(fp2, "%f \t %f \t %f \t %f\n", x*dxy, y*dxy, (stream[x][y+1] - stream[x][y]) / dxy, 
																 (stream[x+1][y] - stream[x][y]) / dxy);
      	}
      	fprintf(fp2, "\n");
    }

	fclose(fp1);
	fclose(fp2);
}

void initStream150(double array[301][91], double Q = -150){
	int i, j;
	for (i = 0; i <= 300; ++i){
		array[i][0]  = streamReal(0, Q);
		array[i][90] = streamReal(90, Q);
	}
	for (j = 0; j <= 90; ++j)
		array[0][j] = array[300][j]  = streamReal(j, Q);	

	for (i = 1; i < 300; ++i)
		for (j = 1; j < 90; ++j)
			array[i][j] = streamReal(j, Q);
}
void initVorticity150(double array[301][91], double Q = -150){
	int i, j;
	for (i = 0; i <= 300; ++i){
		array[i][0]  = vorticityReal(0, Q);
		array[i][90] = vorticityReal(90, Q);
	}
	for (j = 0; j <= 90; ++j)
		array[0][j] = array[300][j]  = vorticityReal(j, Q);	

	for (i = 1; i < 300; ++i)
		for (j = 1; j < 90; ++j)
			array[i][j] = vorticityReal(j, Q);
}

void zad2_150(){
	int x, y;
	double stream[301][91];
	double vorticity[301][91];
	FILE* fp1;
	FILE *fp2;
	fp1 = fopen("2_150psi.dat", "w");
	fp2 = fopen("2_150u.dat", "w");
	Hidrance hidrance(Point(85, 70), Point(101, 70), Point(101, 50), Point(116, 50));

	initStream150(stream, -150);
	initVorticity150(vorticity, -150);

	double streampre = 10, vortpre = 10;
	int it = 0;
	while(fabs(streampre - stream[145][45])  > tol ||
		  fabs(vortpre - vorticity[145][45]) > tol ||   it < 150){
		++it;
		streampre = stream[145][45];
		vortpre   = vorticity[145][45];
		initStreamHidrance(vorticity, stream, hidrance, -150);
		initVorticityHidrance(vorticity, stream, hidrance, -150);
		
		calcStream2(stream, vorticity, hidrance);
		calcVorticity2(stream, vorticity, hidrance);
		// printf("%f %f \n", fabs(streampre - stream[145][45]), fabs(vortpre - vorticity[145][45]));
	}
	for(int x = 1; x < 301; x++){
    	for(int y = 1; y < 91; y++){
			fprintf(fp1, "%f \t %f \t %f\n", x*dxy, y*dxy, stream[x][y]);
      	}
      	fprintf(fp1, "\n");
    }

    for(int x = 1; x < 301; x++){
		for(int y = 1; y < 91; y++){
			fprintf(fp2, "%f \t %f \t %f \t %f\n", x*dxy, y*dxy, (stream[x][y+1] - stream[x][y]) / dxy, 
																 -(stream[x+1][y] - stream[x][y]) / dxy);
	  	}
  	fprintf(fp2, "\n");
    }

	fclose(fp1);
	fclose(fp2);
}

void funcfor400(double stream[301][91], double vorticity[301][91], Hidrance& hidrance){
	for (int i = 1; i < 300; ++i)
		for (int j = 1; j < 90; ++j){
			if (!isHidrance(i-1, j, hidrance) && 
				!isHidrance(i+1, j, hidrance) && 
				!isHidrance(i, j+1, hidrance) &&
				!isHidrance(i, j-1, hidrance) &&
				!isHidrance(i, j, hidrance))
			vorticity[i][j] = (vorticity[i-1][j] + 
						       vorticity[i+1][j] + 
						       vorticity[i][j-1] + 
						       vorticity[i][j+1]) / 4.0  - 
						       (1/16.0) * (
						       		(stream[i][j+1] - stream[i][j-1]) * (vorticity[i+1][j] - vorticity[i-1][j]) - 
						       		(stream[i+1][j] - stream[i-1][j]) * (vorticity[i][j+1] - vorticity[i][j-1])	
						       			);
			if (!isHidrance(i-1, j, hidrance) && 
				!isHidrance(i+1, j, hidrance) && 
				!isHidrance(i, j+1, hidrance) &&
				!isHidrance(i, j-1, hidrance) &&
				!isHidrance(i, j, hidrance))
			stream[i][j] = (stream[i-1][j] + 
						    stream[i+1][j] + 
						    stream[i][j-1] + 
						    stream[i][j+1] -
						    vorticity[i][j] * 0.0001) / 4.0;
	}

}
void zad2_400(){
	int x, y;
	double stream[301][91];
	double vorticity[301][91];
	FILE* fp1;
	FILE *fp2;
	fp1 = fopen("2_400psi.dat", "w");
	fp2 = fopen("2_400u.dat", "w");
	Hidrance hidrance(Point(85, 70), Point(101, 70), Point(101, 50), Point(116, 50));

	initStream150(stream, -400);
	initVorticity150(vorticity, -400);

	double streampre = 10, vortpre = 10;
	int it = 0;
	while(fabs(streampre - stream[145][45])  > tol ||
		  fabs(vortpre - vorticity[145][45]) > tol ||   it < 150){
		++it;
		initStreamHidrance(vorticity, stream, hidrance, -400);
		initVorticityHidrance(vorticity, stream, hidrance, -400);
		streampre = stream[145][45];
		vortpre   = vorticity[145][45];
		funcfor400(stream, vorticity, hidrance);
		// calcStream2(stream, vorticity, hidrance);
		// calcVorticity2(stream, vorticity, hidrance);
		// printf("%f %f \n", fabs(streampre - stream[145][45]), fabs(vortpre - vorticity[145][45]));
	}
	for(int x = 1; x < 301; x++){
    	for(int y = 1; y < 91; y++){
			fprintf(fp1, "%f \t %f \t %f\n", x*dxy, y*dxy, stream[x][y]);
      	}
      	fprintf(fp1, "\n");
    }

	for(int x = 1; x < 301; x++){
		for(int y = 1; y < 91; y++){
			fprintf(fp2, "%f \t %f \t %f \t %f\n", x*dxy, y*dxy, (stream[x][y+1] - stream[x][y]) / dxy, 
																 -(stream[x+1][y] - stream[x][y]) / dxy);
	  	}
  		fprintf(fp2, "\n");
    }

	fclose(fp1);
	fclose(fp2);
}
int main(){


	zad1();
	zad2_1( );
	zad2_150();
	zad2_400();
	return 0;
}