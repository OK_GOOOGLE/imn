set term png size 1800,600
# 1
set out "1_1.png"
set view map
#set contours
#set contour base
#set cntrparam level 30
unset clabel
splot "1_1.dat" u 1:2:3 w pm3d lt -1

# 2
set out "1_2.png"
set view map
#set contours
#set contour base
#set cntrparam level 30
unset clabel
splot "1_2.dat" u 1:2:3 w pm3d lt -1

# 3
set out "1_3.png"
set view map
#set contours
#set contour base
#set cntrparam level 30
unset clabel
splot "1_3.dat" u 1:2:3 w pm3d lt -1

# 4
set out "1_4.png"
set view map
#set contours
#set contour base
#set cntrparam level 30
unset clabel
splot "1_4.dat" u 1:2:3 w pm3d lt -1

# 5
set out "1_5.png"
set view map
#set contours
#set contour base
#set cntrparam level 30
unset clabel
splot "1_5.dat" u 1:2:3 w pm3d lt -1