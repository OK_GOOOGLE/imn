reset

ctrl_format = "%1d.dat"         # pliki zawierające 3 kolumny:   1: t      2: I(t)   3: <x>(t)

frame_format = "%1d_%1d.map"    # pliki zawierające 3 kolumny:   1: x      2: y      3: ro(x,y,t)

first_frame = 0

################################

set term png size 1000,600 enhanced

################################

do for [exercise = 1:3] {
  
  unset title
  
  set size noratio
  set xlabel "t"
  set format x "%.0f"
  
  set xrange [0:100]
  set yrange [*:*]
  
  filename = sprintf(ctrl_format, exercise)
  
  set out sprintf("ex_%1d_I.png", exercise)
  set ylabel "I"
  
  if (exercise == 1) { 
    set format y "%.6f"
    p filename u 1:2 w d notitle
  }
  
  if (exercise == 2) {
    set format y "%.4f"
    p filename u 1:2 w l notitle
  }
  
  if (exercise == 3) {
    set format y "%.2f"
    p filename u 1:2 w l notitle
  }
  
  

  set out sprintf("ex_%1d_x.png", exercise)
  set ylabel "<x>"
  set format y "%.1f"
  p filename u 1:3 w l lw 3 notitle

  ################################

  set size ratio -1
  set xlabel "x"
  set ylabel "y"

  set format x "%.1f"
  set format y "%.1f"
  set format cb "%.1f"
  if (exercise == 3) {
    set format cb "%.1t×10^{%T}"
  }
  
  set xrange [0:3.0]
  set yrange [0:0.9]

  do for [frame = first_frame:5] {
    
    set output sprintf("ex_%1d_%1d.png", exercise, frame)
    set title sprintf("%1d", frame)
    
    filename = sprintf(frame_format, exercise, frame)
    
    p filename u 1:2:3 w image

  }
  
  set out
}
