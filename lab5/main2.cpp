#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#define PI 3.14159265358979323846
const double tol = 1e-7;
const double dxy = 0.01;
const double ymin = 0.0;
const double ymax = 0.9;


class Point{
public:
	int x;
	int y;

	Point(int newX, int newY): x(newX), y(newY) {}
};

class Hidrance{
public:
	Point A;
	Point B;
	Point C;
	Point D;

	Hidrance(Point newA, Point newB, Point newC, Point newD): A(newA), B(newB),
															  C(newC), D(newD) {}
};

bool isHidrance(int x, int y, Hidrance& hidrance){
		if (x > hidrance.A.x && y > hidrance.B.y && x < hidrance.B.x) { return true; }
		if (x > hidrance.C.x && x < hidrance.D.x && y > hidrance.D.y)  { return true; }
		return false;
}

int mod2 (int a){
	if (a == -1) return 300;
	else if (a == 301) return 0;
	
	return a;
}

inline double sqr(const double& a){
	return a*a;
}

inline double cube(const double& a){
	return a*a*a;
}

void initUV(double u[301][91], double v[301][91]){
	int i,j;
	for(int i = 0; i <= 300; ++i)
		for (int j = 0; j <= 90; ++j){
			u[i][j] = -0.5 * (j*dxy - ymin) * (j*dxy - ymax);
			v[i][j] = 0.0;
		}
}

double maxu(double u[301][91], double v[301][91]){
	double max = u[0][0];
	double candidate;
	for(int i = 0; i <= 300; ++i)
		for (int j = 0; j <= 90; ++j){
			candidate = sqrt(sqr(u[i][j]) + sqr(v[i][j]));
			if (candidate > max) {max = candidate;}
		}

	return max;
}

void copyarr(double lhs[301][91], double rhs[301][91]){
	for(int i = 0; i <= 300; ++i)
				for (int j = 0; j <= 90; ++j)
					lhs[i][j] = rhs[i][j];
}


void initDensity_pre(double density_pre[301][91]){
	for (int i = 0; i <= 300; ++i)
		for (int j = 0; j <= 90; ++j)
			density_pre[i][j] = exp(-25 * (sqr(i*dxy - 0.4) + sqr(j*dxy - 0.45)));	
}

void initDensity(double density[301][91], double u[301][91], double v[301][91], double dTzad1){
	for (int i = 0; i <= 300; ++i)
		for (int j = 0; j <= 90; ++j)
			density[i][j] = exp(-25 * (sqr(i*dxy - u[i][j] * dTzad1 - 0.4) +
									   sqr(j*dxy - v[i][j] * dTzad1 - 0.45)
									   )
								);	
}

void leapfrog(double density_pre[301][91], double density[301][91], double density_next[301][91], double u[301][91], double v[301][91], double dTzad1){
	for (int i = 0; i <= 300; ++i)
		for (int j = 1; j < 90; ++j)
			density_next[i][j] = density_pre[i][j] - dTzad1 * (
																	u[i][j] * (density[mod2(i+1)][j] - density[mod2(i-1)][j]) / dxy +
																	v[i][j] * (density[i][j+1] - density[i][j-1]) / dxy
															  );
}

double checkIntegr(double dens[301][91]){
	double value = 0;
	int i,j;
	for(i = 0; i <= 300; ++i)
		for (j = 0; j <= 90; ++j)
			value += dens[i][j] * dxy * dxy;
	return value;
}

double checkPack(double dens[301][91], double integ){
	double value = 0;
	double localinteg = 0;
	int i, j;
	for(i = 0; i <= 300; ++i)
		for (j = 0; j <= 90; ++j)
			localinteg += i*dxy * dens[i][j] * dxy * dxy;
	return localinteg / integ;
}

void printFile(FILE* name, double dens[301][91]){
	for(int i = 0; i <= 300; ++i){
				for (int j = 0; j <= 90; ++j)
					fprintf(name, "%f \t %f \t %f \n", i*dxy, j*dxy, dens[i][j]);
			fprintf(name, "\n");
	}
}

void zad1(){
	double density[301][91];
	double density_pre[301][91];
	double density_next[301][91];
	double u[301][91];
	double v[301][91];
	double integ, pack;
	FILE* fp1;
	FILE* fp2;
	FILE* fp3;
	FILE* fp4;
	FILE* fp5;
	FILE* fp6;
	FILE* fp_1;
	fp1 = fopen("1_0.map", "w");
	fp2 = fopen("1_1.map", "w");
	fp3 = fopen("1_2.map", "w");
	fp4 = fopen("1_3.map", "w");
	fp5 = fopen("1_4.map", "w");
	fp6 = fopen("1_5.map", "w");
	fp_1 = fopen("1.dat", "w");
	initUV(u,v);
	
	double dTzad1 = dxy / (4 * maxu(u, v));
	initDensity_pre(density_pre);
	initDensity(density, u, v, dTzad1);
	int f1, f2, f3, f4, f5, f6;
	f1 = f2 = f3 = f4 = f5 = f6 = 0;
	for (double t = 0; t <= 101; t+=dTzad1 ){
		leapfrog(density_pre, density , density_next, u, v, dTzad1);
		copyarr(density_pre, density);
		copyarr(density, density_next);
		if (t > 0   && t < 0.5   &&  f1 == 0) {printFile(fp1, density_next); ++f1;}
		if (t > 5   && t < 5.5   &&  f2 == 0) {printFile(fp2, density_next); ++f2;}
		if (t > 10  && t < 10.5  &&  f3 == 0) {printFile(fp3, density_next); ++f3;} 
		if (t > 15  && t < 15.5  &&  f4 == 0) {printFile(fp4, density_next); ++f4;} 
		if (t > 20  && t < 20.5  &&  f5 == 0) {printFile(fp5, density_next); ++f5;}
		if (t > 99  && t < 99.5  &&  f6 == 0) {printFile(fp6, density_next); ++f6;}
		
		integ = checkIntegr(density_next);
		fprintf(fp_1, "%f \t %f \t %f \n", t, integ, checkPack(density_next, integ));
	}

	fclose(fp_1);
	fclose(fp1);
	fclose(fp2);
	fclose(fp3);
	fclose(fp4);
	fclose(fp5);
	fclose(fp6);
}

void initUV2(int isHidrance[301][91], double u[301][91], double v[301][91]){
	FILE* f = fopen("dane_5a.dat", "r");
	int i, j, hidr;
	double uu, vv;
	while(fscanf(f, "%d %d %lf %lf %d", &i, &j, &uu, &vv, &hidr) > 0){	
		isHidrance[i][j] = hidr;
		u[i][j] = uu;
		v[i][j] = vv;
		// printf("%d %d %f %f %d\n",i, j, u[i][j], v[i][j], isHidrance[i][j]);
	}
	fclose(f);
}

void initDensity_pre2(int isHidrance[301][91], double density_pre[301][91]){
	for (int i = 0; i <= 300; ++i)
		for (int j = 0; j <= 90; ++j)
			if (isHidrance[i][j])
				density_pre[i][j] = exp(-25. * (sqr(i*dxy - 0.4) + sqr(j*dxy - 0.45)));
			else density_pre[i][j] = 0.0;
}

void initDensity2(int isHidrance[301][91], double density[301][91], double u[301][91], double v[301][91], double dTzad1){
	for (int i = 0; i <= 300; ++i)
		for (int j = 0; j <= 90; ++j){
			if (i == 295 && j == 0) printf ("%f \t %f \n ", u[i][j], v[i][j]);
				if (isHidrance[i][j])
					
					density[i][j] = exp(-25. * (sqr(i*dxy - u[i][j] * dTzad1 - 0.4) +
											   sqr(j*dxy - v[i][j] * dTzad1 - 0.45)
											   )
										);	
				else density[i][j] = 0.0;
		}
}
void leapfrog2(int isHidrance[301][91], double density_pre[301][91], double density[301][91], double density_next[301][91], double u[301][91], double v[301][91], double dTzad2){
	for (int i = 0; i <= 300; ++i)
		for (int j = 1; j < 90; ++j)
			if (isHidrance[i][j] && 
				isHidrance[i+1][j] &&
				isHidrance[i-1][j] &&
				isHidrance[i][j+1] &&
				isHidrance[i][j-1])
				density_next[i][j] = density_pre[i][j] - dTzad2 * (
																		u[i][j] * (density[mod2(i+1)][j] - density[mod2(i-1)][j]) / dxy +
																		v[i][j] * (density[i][j+1] - density[i][j-1]) / dxy
																  );
			else density_next[i][j] = 0.0;
}

double checkIntegr2(int isHidrance[301][91], double dens[301][91]){
	double value = 0;
	int i,j;
	for(i = 0; i <= 300; ++i)
		for (j = 0; j <= 90; ++j)
			if (isHidrance[i][j])
				value += dens[i][j] * dxy * dxy;
	return value;
}

double checkPack2(int isHidrance[301][91], double dens[301][91], double integ){
	double value = 0;
	double localinteg = 0;
	int i, j;
	for(i = 0; i <= 300; ++i)
		for (j = 0; j <= 90; ++j)
			if (isHidrance[i][j])
				localinteg += i*dxy * dens[i][j] * dxy * dxy;
	return localinteg / integ;
}

void zad2(){
	FILE* fp1;
	FILE* fp2;
	FILE* fp3;
	FILE* fp4;
	FILE* fp5;
	FILE* fp6;
	FILE* fp_1;
	fp1 = fopen("2_0.map", "w");
	fp2 = fopen("2_1.map", "w");
	fp3 = fopen("2_2.map", "w");
	fp4 = fopen("2_3.map", "w");
	fp5 = fopen("2_4.map", "w");
	fp6 = fopen("2_5.map", "w");
	fp_1 = fopen("2.dat", "w");

	double density[301][91];
	double density_pre[301][91];
	double density_next[301][91];
	int isHidrance[301][91];
	double u[301][91];
	double v[301][91];
	double integ, pack, dTzad2;

	Hidrance hidrance(Point(85, 70), Point(101, 70), Point(101, 50), Point(116, 50));
	initUV2(isHidrance, u, v);
	dTzad2 = dxy / (4 * maxu(u, v));
	initDensity_pre2(isHidrance, density_pre);
	initDensity2(isHidrance, density, u, v, dTzad2);
	int f1, f2, f3, f4, f5, f6;
	f1 = f2 = f3 = f4 = f5 = f6 = 0;
	for (double t = 0; t <= 101; t+=dTzad2 ){
		leapfrog2(isHidrance, density_pre, density , density_next, u, v, dTzad2);
		copyarr(density_pre, density);
		copyarr(density, density_next);

		if (t > 0  && t < 0.5  && f1 == 0) {printFile(fp1, density_next); ++f1;}
		if (t > 5  && t < 5.5  && f2 == 0) {printFile(fp2, density_next); ++f2;}
		if (t > 10 && t < 10.5 && f3 == 0) {printFile(fp3, density_next); ++f3;} 
		if (t > 15 && t < 15.5 && f4 == 0) {printFile(fp4, density_next); ++f4;} 
		if (t > 20 && t < 20.5 && f5 == 0) {printFile(fp5, density_next); ++f5;}
		if (t > 99 && t < 99.5 && f6 == 0) {printFile(fp6, density_next); ++f6;}

		integ = checkIntegr2(isHidrance, density_next);
		fprintf(fp_1, "%f \t %f \t %f \n", t, integ, checkPack2(isHidrance, density_next, integ));
	}

	fclose(fp_1);
	fclose(fp1);
	fclose(fp2);
	fclose(fp3);
	fclose(fp4);
	fclose(fp5);
	fclose(fp6);
}

void Lax_Fried(int isHidrance[301][91], double density_pre[301][91], double density[301][91], double density_next[301][91], double u[301][91], double v[301][91], double dTzad2){
	for (int i = 0; i <= 300; ++i)
		for (int j = 1; j < 90; ++j)
			if (isHidrance[i][j])
				density_next[i][j] = (density[mod2(i+1)][j] + density[mod2(i-1)][j] + density[i][j-1] + density[i][j+1]) / 4.0 - 
													    dTzad2 * (
																		u[i][j] * (density[mod2(i+1)][j] - density[mod2(i-1)][j]) / (2.0*dxy) +
																		v[i][j] * (density[i][j+1] - density[i][j-1]) / (2.0*dxy)
																  );
			else density_next[i][j] = 0.0;
}

void printarr(int lhs[301][91]){
	for(int i = 0; i <= 300; ++i){
		for (int j = 0; j <= 90; ++j)
			printf("%d ", lhs[i][j]);
		printf("\n");
	}
}

void zad3(){
	FILE* fp1;
	FILE* fp2;
	FILE* fp3;
	FILE* fp4;
	FILE* fp5;
	FILE* fp6;
	FILE* fp_1;
	fp1  = fopen("3_0.map", "w");
	fp2  = fopen("3_1.map", "w");
	fp3  = fopen("3_2.map", "w");
	fp4  = fopen("3_3.map", "w");
	fp5  = fopen("3_4.map", "w");
	fp6  = fopen("3_5.map", "w");
	fp_1 = fopen("3.dat", "w");

	double density[301][91];
	double density_pre[301][91];
	double density_next[301][91];
	int isHidrance[301][91];
	double u[301][91];
	double v[301][91];
	double integ, pack, dTzad2;

	Hidrance hidrance(Point(85, 70), Point(101, 70), Point(101, 50), Point(116, 50));
	initUV2(isHidrance, u, v);
	// printarr(isHidrance);
	dTzad2 = dxy / (4.0 * maxu(u, v));
	dTzad2 = dTzad2 / 2.0;
	initDensity_pre2(isHidrance, density_pre);
	initDensity2(isHidrance, density, u, v, dTzad2);
	density[295][0] = 0.0; // WTF....... z jakiegos powodu ten punkt wypalniony jest smieciami. tylko jeden
	int f1, f2, f3, f4, f5, f6;
	f1 = f2 = f3 = f4 = f5 = f6 = 0;

	// for(int i = 0; i <= 300; ++i){
	// 			for (int j = 0; j <= 90; ++j)
	// 				printf("%d \t %d \t %f \n", i, j, density[i][j]);
	// 		printf("\n");
	// }
	// printFile(fp1, density);

	for (double t = 0; t <= 101; t+=dTzad2 ){
		Lax_Fried(isHidrance, density_pre, density , density_next, u, v, dTzad2);
		// copyarr(density_pre, density);
		copyarr(density, density_next);

		if (t > 0  && t < 0.5  && f1 == 0) {printFile(fp1, density_next); ++f1;}
		if (t > 5  && t < 5.5  && f2 == 0) {printFile(fp2, density_next); ++f2;}
		if (t > 10 && t < 10.5 && f3 == 0) {printFile(fp3, density_next); ++f3;} 
		if (t > 15 && t < 15.5 && f4 == 0) {printFile(fp4, density_next); ++f4;} 
		if (t > 20 && t < 20.5 && f5 == 0) {printFile(fp5, density_next); ++f5;}
		if (t > 99 && t < 99.5 && f6 == 0) {printFile(fp6, density_next); ++f6;}

		integ = checkIntegr2(isHidrance, density_next);
		fprintf(fp_1, "%f \t %f \t %f \n", t, integ, checkPack2(isHidrance, density_next, integ));
	}

	fclose(fp_1);
	fclose(fp1);
	fclose(fp2);
	fclose(fp3);
	fclose(fp4);
	fclose(fp5);
	fclose(fp6);
}

int main(){
	zad1();
	zad2();
	zad3();
}