set term png
set out "1_integr.png"
set logscale x

plot "1_integr.dat" u 1:2 w l

unset logscale x

set out "1.png"
set view map
set contours
set contour base
set cntrparam level 30

unset clabel
splot "1_1.dat" u 1:2:3 w pm3d lt -1 

set out "2_integr.png"
set logscale x
plot "2_integr.dat" u 1:2 w l


unset logscale x
set out "2.png"
set view map
set size ratio -1
set contours
set contour base
set cntrparam levels 30
unset clabel
splot "2_1.dat" u 1:2:3 w pm3d lt -1