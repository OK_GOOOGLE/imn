reset

file = "psi.map"    # plik zawierający 3 kolumny:
                    # 1: x,   2: y,   3: psi(x,y)


                        
set term png size 1000,600 enhanced
set out "1_psi.png"

set xlabel "x"
set ylabel "y"

set xrange [1:200]
set yrange [1:100]

set size ratio -1

set contour
set cntrparam levels incremental 5,5,199
set view map
unset surface
unset key

set object polygon from 85,100 to 85,85 to 100,85 to 100,70 to 115,70 to 115,100 fs solid border lc rgb "black" fc rgb "gray" front

sp file u 1:2:3 w l notitle