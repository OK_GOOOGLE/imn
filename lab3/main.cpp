#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#define PI 3.14159265358979323846
double tol = 1e-6;

double sqr(double a){
	return a*a;
}

bool isHidrance(int x, int y){
	if (x > 100 && y > 70 && x < 115) { return true; }
	if (x > 85 && x < 100 && y > 85)  { return true; }
	return false;
}

void initArray(double array[201][101]){
	int i, j;
	for (i = 1; i <= 200; ++i){
		for (j = 1; j <= 100; ++j)
			array[i][j] = 0.0;
	}
	for (i = 1; i < 101; ++i){ //lewy i prawy brzegi
		array[1][i] = i;
		array[200][i] = i;
	}

	for (i = 1; i < 201; ++i) { // dol i gora
		array[i][1] = 0;
		if (!isHidrance(i, 100)){
			array[i][100] = 100;
		}
	}

	//AB i lewy brzeg preszkody
	for (i = 85; i <= 100; ++i){ 
		array[i][85] = 100.0;
	}
	for (i = 85; i <= 100; ++i){ 
		array[85][i] = 100.0;
	}
	
	//BC
	for (i = 70; i <= 85; ++i){
		array[100][i] = 100.0;
	}
	//CD
	for (i = 100; i <= 115; ++i){
		array[i][70] = 100.0;
	}
	//prawy brzeg
	for(i = 70; i <= 100; ++i){
		array[115][i] = 100.0;
	}
}

void calcPsi(int x, int y, double array[201][101]){
	array[x][y] = (array[x-1][y] + 
				   array[x+1][y] + 
				   array[x][y-1] + 
				   array[x][y+1]) / 4.0;
}

void psi(double array[201][101]){
	int x, y;
	for(x = 2; x < 200; ++x){
		for(y = 2; y < 100; ++y){
			if (!isHidrance(x-1, y) && 
				!isHidrance(x+1, y) && 
				!isHidrance(x, y+1))
			calcPsi(x, y, array);
		}
	}
}

double calcIntegral(double array[201][101]){
	int x, y;
	double value = 0.0;
	for(x = 2; x < 200; ++x){
		for(y = 2; y < 100; ++y){
			if (!isHidrance(x-1, y) && 
				!isHidrance(x+1, y) && 
				!isHidrance(x, y+1))

				value += sqr(array[x+1][y] - array[x-1][y]) + 
					     sqr(array[x][y+1] - array[x][y-1]);	
		}
	}
	return value / 8.0;
}

void zad1(){
	int x, y;
	double array[201][101];
	FILE* fp1;
	FILE *fp2;
	fp1 = fopen("1_1.dat", "w");
	fp2 = fopen("1_integr.dat", "w");
	initArray(array);

	double integ, integ_pre;
	
	psi(array);
	integ_pre = calcIntegral(array);
	psi(array);
	integ = calcIntegral(array);
	int it = 1;
	fprintf(fp2, "%d\t%f\n", it, integ);
	while (fabs(integ - integ_pre) > tol && it < 10000){
		integ_pre = integ;
		psi(array);
		integ = calcIntegral(array);
		++it;
	fprintf(fp2, "%d\t%f\n", it, integ);
	}

	for(int x = 1; x < 201; x++){
    	for(int y = 1; y < 101; y++){
			fprintf(fp1, "%d \t %d \t %f\n", x, y, array[x][y]);
      	}
      	fprintf(fp1, "\n");
    }
	fclose(fp1);
	fclose(fp2);
}

void initArray2(double array[201][101]){
	int i, j;
	for (i = 1; i <= 200; ++i){
		for (j = 1; j <= 100; ++j)
			if (!isHidrance(i, j))
				array[i][j] = i;
	}
}

void neuman(double array[201][101]){
	int x, y, i, j;
	//gora
	for (i = 0; i <= 84; ++i)
		array[i][100] = array[i][99];
	for (i = 116; i <= 200; ++i)
		array[i][100] = array[i][99];
	//dol
	for (i = 1; i <= 200; ++i)
		array[i][1] = array[i][2];

	//a)
	for(int j = 86; j <= 100; ++j)
    	array[85][j] = array[84][j];
    
	//b)
  	for(int j = 71; j < 85; ++j)
    	array[100][j]=array[99][j];

    //c)
  	for(int j = 70; j <= 100; ++j)
    	array[115][j]=array[116][j];

    //d)
	for(int i = 86; i < 100; ++i)
    	array[i][85]=array[i][84];

    //e)
  	for(int i = 101; i < 115; ++i)
    	array[i][70]=array[i][69];
    

    //A
    array[85][85] = (array[85][84] + array[84][85]) / 2.0;
    //B
    array[100][85] = array[99][84];
    //C
    array[100][70] = (array[99][70] + array[100][69]) / 2.0;
    //D
    array[115][70] = (array[116][70] + array[115][69]) / 2.0;
}

void zad2(){
	FILE* fp1;
	FILE *fp2;
	fp1 = fopen("2_1.dat", "w");
	fp2 = fopen("2_integr.dat", "w");

	double array[201][101];

	initArray2(array);
	neuman(array);
	psi(array);

	double integ, integ_pre;
	integ_pre = calcIntegral(array);
	neuman(array);
	psi(array);
	integ = calcIntegral(array);
	int it = 1;
	fprintf(fp2, "%d\t%f\n", it, integ);
	while (fabs(integ - integ_pre) > tol && it < 15000){
		integ_pre = integ;
		neuman(array);
		psi(array);
		integ = calcIntegral(array);
		++it;
	fprintf(fp2, "%d\t%f\n", it, integ);
	}

	for(int x = 1; x < 201; x++){
    	for(int y = 1; y < 101; y++){
			fprintf(fp1, "%d \t %d \t %f\n", x, y, array[x][y]);
      	}
      	fprintf(fp1, "\n");
    }
	fclose(fp1);
	fclose(fp2);
}


int main(){


	zad1();
	zad2();

	return 0;
}