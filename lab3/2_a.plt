reset

file = "fi.dat"    # plik zawierający 2 kolumny:
                   # 1: aktualny numer iteracji it,   2: a(it)


                        
set term png size 1000,600 enhanced
set out "2_a.png"

set xlabel "iteracja"
set ylabel "a"

set logscale x

p file u 1:2 w l lw 3 notitle