#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#define PI 3.14159265358979323846
const double tol = 1e-8;
const double dxy = 1.0;
const double ymin = 0.0;
const double ymax = 0.9;



inline double sqr(const double& a){
	return a*a;
}

inline double cube(const double& a){
	return a*a*a;
}

void copyarr(double lhs[101][51], double rhs[101][51]){
	for(int i = 0; i <= 100; ++i)
				for (int j = 0; j <= 50; ++j)
					lhs[i][j] = rhs[i][j];
}


void printFile(FILE* name, double T[101][51]){
	for(int i = 0; i <= 100; ++i){
				for (int j = 0; j <= 50; ++j)
					fprintf(name, "%d \t %d \t %f \n", i, j, T[i][j]);
			fprintf(name, "\n");
	}
}

void printarr(int lhs[101][51]){
	for(int i = 0; i <= 300; ++i){
		for (int j = 0; j <= 90; ++j)
			printf("%d ", lhs[i][j]);
		printf("\n");
	}
}
const double sqrt2 = 1.4142135623;
void initTemper(double T[101][51], double& h){ // dont forget to init T1, T2, T0
	/*T0 = 0; T2 = T1 = 21*/
	/*I, D*/
	for (int j = 20; j <= 50; ++j) {   T[40][j] = T[39][j] / (h + 1.0);	}
	for (int j = 0; j <= 30; ++j)  {   T[80][j] = T[79][j] / (h + 1.0);	}
	/*B, G*/   
	for (int j = 0; j <= 30; ++j)  {   T[20][j] = T[21][j] / (h + 1.0);	}
	for (int j = 20; j <= 50; ++j) {   T[60][j] = T[61][j] / (h + 1.0);	}
	/* A, E, C*/   
	for (int i = 0; i <= 20; ++i)  {   T[i][30] = T[i][31] / (h + 1.0);	}
	for (int i = 80; i <= 100; ++i){   T[i][30] = T[i][31] / (h + 1.0);	}
	for (int i = 0; i <= 100; ++i){   T[i][0] = T[i][1] / (h + 1.0);	}
	/*J, H, F*/
	for (int i = 0; i <= 40; ++i)  {   T[i][50] = T[i][49] / (h + 1.0); }
	for (int i = 40; i <= 60; ++i) {   T[i][20] = T[i][19] / (h + 1.0); }
	for (int i = 60; i <= 100; ++i){   T[i][50] = T[i][49] / (h + 1.0); }
	/*narozniki*/
	/* J/I , I/H */
	T[40][50] = (T[39][49] / sqrt2) / (h + 1.0 / sqrt2);
	T[40][20] = (T[39][19] / sqrt2) / (h + 1.0 / sqrt2);
	/* F/G, G/H */
	T[60][50] = (T[61][49] / sqrt2) / (h + 1.0 / sqrt2);
	T[60][20] = (T[61][19] / sqrt2) / (h + 1.0 / sqrt2);
	/* E/D, D/C */
	T[80][30] = (T[79][31] / sqrt2) / (h + 1.0 / sqrt2);
	T[80][0]  = (T[79][1]  / sqrt2) / (h + 1.0 / sqrt2);
	/* A/B, B/C */
	T[20][30] = (T[21][31]  / sqrt2) / (h + 1.0 / sqrt2);
	T[20][0]  = (T[21][1]   / sqrt2) / (h + 1.0 / sqrt2);

	/*T1, T2*/
	for (int j = 30; j <= 50; ++j) {
		T[0][j] = T[100][j] = 21.0;
	}
}

const int imin = 0, i_1 = 20, i_2 = 40, i_3 = 60, i_4 = 80, imax = 100;
const int jmin = 0, j_1 = 20, j_2 = 30, jmax = 50;

int insideTheRoom(int i, int j){
	if ((i <=i_1 && j <= j_2) ||          /* left  */
       (i>=i_2 && i<=i_3 && j>=j_1) || /* centre  */
       (i>=i_4 && j<=j_2)) return 0;   /* right  */
    return 1;
}

double calcInteg(double T[101][51]){
	double sum = 0.0;
	for(int i = 0; i <= imax; ++i)
		for (int j = 0; j <= jmax; ++j)
			sum += T[i][j];
	return sum;
}

void calcTemp(double T[101][51], double T_pre[101][51], double dt, double h){
// k, p, c = 1, dxy = 1
	double integ = 0, integ_pre = 0;
	double dI = 100;
	while(fabs(dI) > tol ){
		integ = 0.0;
		initTemper(T, h);
		for(int i = 1; i < 100; ++i)
			for(int j = 1; j < 50; ++j){
				
				if (insideTheRoom(i, j)){

					T[i][j] = (1.0 / (1.0 + 2.0 * dt)) * (T_pre[i][j] + (dt / 2.0) * (
																						T_pre[i][j-1] + T_pre[i][j+1] + 
																						T_pre[i-1][j] + T_pre[i+1][j] - 4.0 * T_pre[i][j] +
																						T[i][j-1] + T[i][j+1] + 
																						T[i-1][j] + T[i+1][j]
																				   )
														 );

				}
				// printf("%d %d %f\n", i, j, T[i][j]);
			}

		integ = calcInteg(T);

		dI = (integ_pre - integ) / integ_pre;
		integ_pre = integ;
	}
}




void stream(double T[101][51], double& streamRed, double& streamBlue){
	streamBlue = 0.0;
	streamRed  = 0.0;
	for (int j = j_2; j <= jmax; ++j){
		streamBlue += (T[2][j]  - T[0][j]) / 2.0;
		streamRed  += (T[100][j] - T[98][j]) / 2.0;
	}
}

void zad1(){
	const double dt = 2.5;
	const double h = 0.0;

	double T[101][51] = {0};
	double T_pre[101][51] = {0};
	for (int j = 40; j <= 50; ++j) {
		T_pre[0][j] = T_pre[100][j] = 21.0;
	}

	double streamRed;
	double streamBlue;
	double integ, integ_pre = -100;
	FILE* fp1;
	FILE* fp2;
	FILE* fp3;
	FILE* fp4;
	FILE* fp5;
	FILE* fp6;
	FILE* fp_1;
	fp1 = fopen("1_1.map", "w");
	// fp2 = fopen("1_1.map", "w");
	// fp3 = fopen("1_2.map", "w");
	// fp4 = fopen("1_3.map", "w");
	// fp5 = fopen("1_4.map", "w");
	// fp6 = fopen("1_5.map", "w");
	fp_1 = fopen("1.dat", "w");



	
	
	int f1, f2, f3, f4, f5, f6;
	f1 = f2 = f3 = f4 = f5 = f6 = 0;
	double dI = 1.0;
	int it = 0;
	for (double t = 0; fabs(dI) > tol; t+= dt){
		calcTemp(T, T_pre, dt, h);
		stream(T, streamRed, streamBlue);
		// if (t > 0   && t < 0.5   &&  f1 == 0) {printFile(fp1, T); ++f1;}
		// if (t > 125  && t < 5.5   &&  f2 == 0) {printFile(fp2, T); ++f2;}
		// if (t > 250  && t < 10.5  &&  f3 == 0) {printFile(fp3, T); ++f3;} 
		// if (t > 500  && t < 15.5  &&  f4 == 0) {printFile(fp4, T); ++f4;} 
		// if (t > 1000  && t < 20.5  &&  f5 == 0) {printFile(fp5, T); ++f5;}
		// if (t > 99  && t < 99.5  &&  f6 == 0) {printFile(fp6, density_next); ++f6;}
		integ = calcInteg(T);
		dI = (integ_pre - integ) / integ_pre;
		//printf("%f %f\n", dI, integ);
		++it;
		printf("%d\n", it);
		integ_pre = integ;
		copyarr(T_pre, T);
		fprintf(fp_1, "%f \t %f \t %f \t %f \n", t, streamRed, -streamBlue, streamRed-streamBlue);
	}
	printFile(fp1, T);

	fclose(fp_1);
	fclose(fp1);
// 	fclose(fp2);
// 	fclose(fp3);
// 	fclose(fp4);
// 	fclose(fp5);
// 	fclose(fp6);
}

void zad2(){
	const double dt = 2.5;
	const double h = 0.01;

	double T[101][51] = {0};
	double T_pre[101][51] = {0};
	for (int j = 40; j <= 50; ++j) {
		T_pre[0][j] = T_pre[100][j] = 21.0;
	}

	double streamRed;
	double streamBlue;
	double integ, integ_pre = -100;
	FILE* fp1;
	FILE* fp2;
	FILE* fp3;
	FILE* fp4;
	FILE* fp5;
	FILE* fp6;
	FILE* fp_1;
	fp1 = fopen("2_1.map", "w");
	fp2 = fopen("2_2.map", "w");
	fp3 = fopen("2_3.map", "w");
	fp4 = fopen("2_4.map", "w");
	fp5 = fopen("2_5.map", "w");
	fp_1 = fopen("2.dat", "w");



	
	
	int f1, f2, f3, f4, f5, f6;
	f1 = f2 = f3 = f4 = f5 = f6 = 0;
	double dI = 1.0;
	int it = 0;
	for (double t = 0; fabs(dI) > tol; t+= dt){
		calcTemp(T, T_pre, dt, h);
		stream(T, streamRed, streamBlue);
		if (t > 0   && t < 9   &&  f1 == 0) {printFile(fp1, T); ++f1;}
		if (t > 125  && t < 129   &&  f2 == 0) {printFile(fp2, T); ++f2;}
		if (t > 250  && t < 259  &&  f3 == 0) {printFile(fp3, T); ++f3;} 
		if (t > 500  && t < 509  &&  f4 == 0) {printFile(fp4, T); ++f4;} 
		if (t > 1000  && t < 1009  &&  f5 == 0) {printFile(fp5, T); ++f5;}
		integ = calcInteg(T);
		dI = (integ_pre - integ) / integ_pre;
		//printf("%f %f\n", dI, integ);
		++it;
		printf("%d\n", it);
		integ_pre = integ;
		copyarr(T_pre, T);
		fprintf(fp_1, "%f \t %f \t %f \t %f \n", t, streamRed, -streamBlue, streamRed-streamBlue);
	}

	fclose(fp_1);
	fclose(fp1);
// 	fclose(fp2);
// 	fclose(fp3);
// 	fclose(fp4);
// 	fclose(fp5);
// 	fclose(fp6);
}

int main(){
	zad1();
	zad2();

}