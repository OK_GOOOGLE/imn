#include <stdio.h>
#include <math.h>

#define PI 3.14159265359

double dokladne(double t){
	return exp(sin(t));
}

double fun(double t, double u){
	return u * cos(t);
}

void zad1(){
	double t_max = 4.0 * PI;
	double dt = PI / 400.0;
	double u0 = 1.0;
	double t = 0;
	double u1;

	FILE *fp;
	fp = fopen("1_1.dat", "w");
	for (t = 0.0; t <= t_max; t+=dt){
		u1 = u0 + dt * fun(t, u0);
		fprintf(fp , "%.4f %.6f %.6f %.6f \n", t, u0, dokladne(t), dokladne(t) - u0);
		u0 = u1;
	}
	fclose(fp);


	fp = fopen("1_2.dat", "w");
	int i;
	for (i = 10; i >= 1; --i){
		dt = PI / (10. * i);
		u0 = 1.0;
		for (t = 0.0; t <= PI - (0.5 * dt); t+=dt){
			u1 = u0 + dt * fun(t, u0);
			u0 = u1;
		}
		fprintf(fp , "%.6f %.6f %.6f \n", dt, u1, dokladne(t) - u1);
	}
	fclose(fp);
}

void zad2(){
	double t_max = 4.0 * PI;
	double dt = PI / 400.0;
	double u0 = 1.0;
	double t = 0;
	double u1;
	FILE *fp;
	fp = fopen("2.dat", "w");

	for (t = 0.0; t <= t_max; t+=dt){
		fprintf(fp , "%.4f %.6f %.6f \n", t, u0, dokladne(t) - u0);
		u1 = u0 / (1 - dt * cos(t+dt));
		u0 = u1;
	}
	fclose(fp);
}



double funPochodna(double t){
		return cos(t);
}

void zad3(){
	double d0 = 1.0;
	double u1 = 1.0;
	double dt = PI / 400.0;
	double u0 = 1.0;
	double u_tmp = u0;
	double t_max = 4.0 * PI;
	double zbieznosc = 0.000001;
	int ilosc = 0;
	double t, eps;
	FILE* fp;
	fp = fopen("3.dat", "w");

	double u_n;
	for (t = dt; t <= t_max; t+=dt){
		u_n = u0;
		u1 = u_tmp;
		eps = 10.0;
		while (eps > zbieznosc && ilosc < 10){
			++ilosc;
			u_n = u1 - (u1 - u_tmp - dt * fun(t, u1)) / 
					   (1 - dt * funPochodna(t));
			eps = fabs(u1 - u_n);
			u1 = u_n;
		}

		fprintf(fp, "%.6f %.6f %d \n", t, dokladne(t) - u_n, ilosc);
		u_tmp = u_n;

		ilosc = 0;
	}



	fclose(fp);
}

double dokladne1(double t){
	return t;
}



void zad4(){
	double u_1, u_12, u_2; // u1 - jeden dlugi krok. 
						   // u_12 - pierwszy krotki krok
						 	//u_2 - drugi krotki krok(obliczamy na podstawie u_12)
	double dt = PI / 400.0;
	double u_0 = 1.0;
	double t;
	double u_poprawione;
	

	FILE* fp;
	fp = fopen("4.dat", "w");

	for (t = 2*dt; t <= 4.0 * PI; t += 2*dt){
		u_1  = u_0  + (2*dt * fun(t - 2*dt,  u_0)); // dlugi krok

		u_12 = u_0  + (dt   * fun(t - 2*dt, u_0));  // krotki krok
		u_2  = u_12 + (dt   * fun(t - dt,   u_12));

		u_poprawione = 2 * u_2 - u_1; // ze wzoru (12) z instrukcji (zakladajac n = 2)

		fprintf(fp, "%.6f %.6f %.6f \n", t, u_poprawione, dokladne(t) - u_poprawione);

		u_0 = u_poprawione;
	}
	fclose(fp);	
}


void zad5(){
	double tol = 10e-5;
	double t = 0, E, S = 0.75, u_poprawione;
	double dt = PI * 5.0;
	double u_0 = 1.0, u_1, u_12, u_2;
	FILE* fp;
	fp = fopen("5.dat", "w");
	while (t <= 4.0 * PI){
		u_1  = u_0  + (2*dt * fun(t,  u_0)); 

		u_12 = u_0  + (dt   * fun(t, u_0));
		u_2  = u_12 + (dt   * fun(t + dt, u_12));

		E = fabs(u_2 - u_1);

		if (E < tol){
			t+=dt; // akceptacja
			u_poprawione = E + u_12;
			fprintf(fp, "%.6f %.6f %.6f %.6f %.6f \n", t, u_poprawione, dokladne(t), dokladne(t) - u_poprawione, dt);
			dt *= sqrt((S * tol) / E);
			u_0 = u_poprawione;
		} else
			dt *= sqrt((S * tol) / E);
	}
	fclose(fp);	

}

double fun1(double t, double x, double y) {
	return y;
}

double fun2(double t, double x, double y) {
	return -x;
}

void zad6(){
	double dt = 0.1;
	double y0 = 1.0;
	double x0 = 0.0;
	double t;
	FILE* fp;

	double k_x[4], k_y[4];
	double t0 = 0.0;
	fp = fopen("6.dat", "w");
	for(t = dt; t <= 4 * PI; t+=dt){

		/*ROZWIAZANIE WEDLUG WZORU*/
		// k_x[0] = fun1(t0, x0, y0);
		// k_y[0] = fun2(t0, x0, y0);
		
		// k_x[1] = fun1(t0 + dt / 2.0, x0 + k_x[0] * dt / 2.0, y0 + k_y[0] * dt / 2.0);
		// k_y[1] = fun2(t0 + dt / 2.0, x0 + k_x[0] * dt / 2.0, y0 + k_y[0] * dt / 2.0);

		// k_x[2] = fun1(t0 + dt / 2.0, x0 + k_x[1] * dt / 2.0, y0 + k_y[1] * dt / 2.0);
		// k_y[2] = fun2(t0 + dt / 2.0, x0 + k_x[1] * dt / 2.0, y0 + k_y[1] * dt / 2.0);

		// k_x[3] = fun1(t0 + dt,       x0 + k_x[2] * dt,       y0 + k_y[2] * dt);
		// k_y[3] = fun2(t0 + dt,       x0 + k_x[2] * dt,       y0 + k_y[2] * dt);

		//to robi trochę mniej obliczeń dla konkretnych fun1 & fun2
		k_x[0] = fun1(1, 1, y0);
		k_y[0] = fun2(1, x0, 1);
		k_x[1] = fun1(1, 1, y0 + k_y[0] * dt / 2.0);
		k_y[1] = fun2(1, x0 + k_x[0] * dt / 2.0, 1);
		k_x[2] = fun1(1, 1, y0 + k_y[1] * dt / 2.0);
		k_y[2] = fun2(1, x0 + k_x[1] * dt / 2.0, 1);
		k_x[3] = fun1(1, 1, y0 + k_y[2] * dt);
		k_y[3] = fun2(1, x0 + k_x[2] * dt, 1);


		x0 = x0 + dt * (k_x[0] + 2 * k_x[1] + 2 * k_x[2] + k_x[3]) / 6.0;
		y0 = y0 + dt * (k_y[0] + 2 * k_y[1] + 2 * k_y[2] + k_y[3]) / 6.0;

		fprintf(fp, "%.8f %.8f %.8f %.8f \n", x0, y0, t, sin(t) - x0);

		t0 = t;
	}
	fclose(fp);
}



//////////////////////////

int main(){
	zad1();
	zad2();
	zad3();
	zad4();
	zad5();
	zad6();
	return 0;
}