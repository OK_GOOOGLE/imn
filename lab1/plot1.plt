set terminal png
set termoption dash
set output "1_1.png"
set autoscale
set xlabel "t"
set ylabel "Wartosc"
set title "zadanie 1_1"
set ytics nomirror
set y2tics  
plot "1_1.dat" u 1:2 w l t "numeryczne u(t)",\
	"1_1.dat" u 1:3 w l t "dokładne u(t)",\
	"1_1.dat" u 1:4 axes x1y2 w l t "e(t)"

set output "1_2.png"
set autoscale
set xlabel "dt"
set ylabel "e(t)"
set title "zadanie 1_2"  
plot "1_2.dat" u 1:3 w l t "blad w zaleznosci od dT"


set output "2.png"
set autoscale
set xlabel "dt"
set ylabel "e(t)"
set ytics nomirror
set y2tics
set title "zadanie 2"  
plot "2.dat" u 1:3 w l t "blad niejawny", \
 	"1_1.dat" u 1:4 w l t "blad jawny"



set output "3_1.png"
set autoscale
set xlabel " t"
set ylabel "e(t)"
set ytics nomirror
set y2tics
set title "Porownanie bledow (zadanie 3)"  
plot "3.dat" u 1:2 axes x1y2 w l t "Blad z metody niejawnej - iteracja Newtona",\
	"2.dat" u 1:3 axes x1y2 w l t "Blad z metody niejawnej",\
	"1_1.dat" u 1:4 axes x1y2 w l t "Blad z metody jawnej"


set output "3_2.png"
set autoscale
set xlabel "t"
set ylabel "ilosc"
set title "iteracje"  
plot "3.dat" u 1:3 w l t "iteracje"

set output "4.png"
set autoscale
set xlabel "t"
set ylabel "e(t)"
set title "zadanie 4"  
plot "4.dat" u 1:3 w l t "blad", \
	  "1_1.dat" u 1:4 w l t "blad z zad 1"


set output "5.png"
set autoscale
set xlabel "t"
set ylabel "Wartosc"
set ytics nomirror
set y2tics
set title "zadanie 5"  
plot "5.dat" u 1:2 w l t "numeryczne", \
	 "5.dat" u 1:3 w l t "dokladne", \
	 "5.dat" u 1:5 axes x1y2 w l t "krok czasowy dT(t)"

set output "6_1.png"
set autoscale
set xlabel "x"
set ylabel "y"
set title "zadanie 6"  
plot "6.dat" u 1:2 w l t "y(x)"
#
#
set output "6_blad.png"
set autoscale
set xlabel "t"
set ylabel "e(t)"
set title "blad zadanie 6"  
plot "6.dat" u 3:4 w l t "blad"
