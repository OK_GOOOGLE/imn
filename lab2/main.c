#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define PI 3.14159265358979323846
const double dx = 0.75, dy = 0.75;
const double xmin = -72, ymin = -72;
const double xmax = 72,  ymax = 72;
const double eps = 1e-8; // 10^-8
int numElem = 193; // dla kwadratowej siatki wymiary sa rowne

double sqr(double x){
	return x * x;
}


void printARRR(double a[193][193]){
	int i, j;
	for(i = 0; i < 50; ++i){
		for(j = 0; j < 50; ++j){
			printf("%.8f ", a[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void copyARRR(double pre[193][193], double cur[193][193]){
	int i, j;
	for(i = 0; i < numElem; ++i){
		for(j = 0; j < numElem; ++j){
			pre[i][j] = cur[i][j];
		}
	}
}

void inicjalizacjaGestosci(double gestosc[][193]){
	int i, j;
	double x, y;
	for(x = xmin, i = 0; i < numElem; x+=dx, ++i){
		for(y = ymin, j = 0; j < numElem; y+=dy, ++j){
			gestosc[i][j] = (1 / (625 * PI)) * exp(-sqr(x / 25) - sqr(y / 25));
		}
	}
}

void newPotencjal(double omega, double gestosc[193][193], double pot_pre[193][193], double pot_cur[193][193]){
	int i, j;
	for(i = 1; i < numElem - 1; ++i){ // bez granic siatki
		for(j = 1; j < numElem - 1; ++j){
			pot_cur[i][j] = (1 - omega) * pot_cur[i][j] + omega * (pot_cur[i+1][j] +
																   pot_cur[i-1][j] + 
																   pot_cur[i][j-1] + 
																   pot_cur[i][j+1] + 
																   gestosc[i][j] * sqr(dx)
																   ) / 4.0;
		}

	}
	
}

double newIntegral(double pot_cur[][193], double gestosc[][193]){
	double value = 0.0;
	int i, j;

	for(i = 0; i < numElem - 1; ++i){
		for(j = 0; j < numElem - 1; ++j){
			value +=  0.5 * (sqr(pot_cur[i+1][j] - pot_cur[i][j]) + 
							 sqr(pot_cur[i][j+1] - pot_cur[i][j])) - gestosc[i][j] * pot_cur[i][j] * dx * dx ;
		}
	}
	return value;
} 


void zad1(){

	double gestosc[193][193] = {0.0};
	// double pot_pre[193][193] = {0.0};
	// double pot_cur[193][193] = {0.0};
	double integral_pre, integral_cur;

	inicjalizacjaGestosci(gestosc); // wzor (2) z instrukcji

	const double omega[7] = {0.6, 0.8, 1.0, 1.5, 1.9, 1.95, 1.99};

	int i, it;
	for (i = 3; i <= 3; ++i){
		double pot_pre[193][193] = {0.0};
		double pot_cur[193][193] = {0.0};
		char name[10] = {'\n'};
		snprintf(name, 5, "%f", omega[i]);
		strcat(name, ".dat");
		FILE* fp = fopen("fi.map", "w"); // change to name for comparison

		newPotencjal(omega[i], gestosc, pot_pre, pot_cur); // wzor (3)
		copyARRR(pot_pre, pot_cur);
		
		integral_pre = newIntegral(pot_cur, gestosc);
		// printf("%f %f\n", pot_pre[10][10], pot_cur[10][10]);
		newPotencjal(omega[i], gestosc, pot_pre, pot_cur);
		copyARRR(pot_pre, pot_cur);
		// printARRR(pot_pre);

		integral_cur = newIntegral(pot_cur, gestosc);
		newPotencjal(omega[i], gestosc, pot_pre, pot_cur);
		copyARRR(pot_pre, pot_cur);
		it = 2;
		printf("%d %f %f\n", it, integral_pre, integral_cur);
		while (fabs(integral_cur - integral_pre) > eps){
			integral_pre = integral_cur;
			integral_cur = newIntegral(pot_cur, gestosc);
			newPotencjal(omega[i], gestosc, pot_pre, pot_cur);
			copyARRR(pot_pre, pot_cur);
			++it;

			 // fprintf(fp, "%d %f\n", it, integral_cur);
		}
		int ik, jk;
		for(ik = 0; ik < numElem - 1; ++ik){
			for(jk = 0; jk < numElem - 1; ++jk){
				fprintf(fp, "%f %f %f\n", -72 + ik*dx, -72 + jk*dx, pot_cur[ik][jk]);
			}
		}


		printf("%d \n", it);

		fclose(fp);
	}

	
}


void newPotencjal2(double omega, double gestosc[193][193], double pot_pre[193][193], double pot_cur[193][193], int k){
	int i, j;
	for(i = 1; i < numElem - 1; ++i){ // bez granic siatki
		for(j = 1; j < numElem - 1; ++j){
			pot_cur[i][j] = (1 - omega) * pot_cur[i][j] + omega * (pot_cur[i+1][j] +
																   pot_cur[i-1][j] + 
																   pot_cur[i][j-1] + 
																   pot_cur[i][j+1] + 
																   gestosc[i][j] * sqr(k*dx)
																   ) / 4.0;
		}
	}
}

double newIntegral2(double pot_cur[][193], double gestosc[][193], int k){
	double value = 0.0;
	int i, j;

	for(i = 0; i < numElem - k; i+=k){
		for(j = 0; j < numElem - k; j+=k){
			value +=  0.5 * (sqr(pot_cur[i+k][j] - pot_cur[i][j]) + 
							 sqr(pot_cur[i][j+k] - pot_cur[i][j])) - gestosc[i][j] * pot_cur[i][j] * sqr(dx*k) ;
		}
	}
	return value;
} 



void zageszczenieSiatki(double pot[193][193], int k){
	int i,j;
	for (i = 1; i < numElem - k; i+=k){
		for (j = 1; j < numElem - k; j+=k){
			printf("%d %d \n", i, j);
			if (pot[i][j] == 0 && (i%(k*2) == 0)){
				pot[i][j] = (pot[i][j-k] + pot[i][j+k]) / 2.0;

			} else
			if (pot[i][j] == 0 && (j%(k*2) == 0)){
				pot[i][j] = (pot[i-k][j] + pot[i+k][j]) / 2.0;
			} else
			if (pot[i][j] == 0 && (j%(k*2) != 0) && (i%(k*2) != 0)){
				pot[i][j] = (pot[i-k][j-k] + pot[i-k][j+k] +  pot[i+k][j-k] +  pot[i+k][j+k]) / 4.0;
			} 
		}
	}
}

void zad2(){

	double gestosc[193][193] = {0.0};
	double integral_pre, integral_cur;
	inicjalizacjaGestosci(gestosc);

	const double omega = 1;
	int k[6] = {32, 16, 8, 4, 2, 1};
	int i, it;
	int ik, jk;

	double pot_pre[193][193] = {0.0};
	double pot_cur[193][193] = {0.0};


	FILE* fp = fopen("32.map", "w");
	newPotencjal2(omega, gestosc, pot_pre, pot_cur, k[0]); // wzor (3)
	integral_pre = newIntegral2(pot_cur, gestosc, k[0]);
	newPotencjal2(omega, gestosc, pot_pre, pot_cur, k[0]);
	integral_cur = newIntegral2(pot_cur, gestosc, k[0]);
	newPotencjal2(omega, gestosc, pot_pre, pot_cur, k[0]);
	it = 2;
	while (fabs(integral_cur - integral_pre) > eps){
		integral_pre = integral_cur;
		integral_cur = newIntegral2(pot_cur, gestosc, k[0]);
		newPotencjal2(omega, gestosc, pot_pre, pot_cur, k[0]);
		
		++it;

		 // fprintf(fp, "%d %f\n", it, integral_cur);
	}



		for(ik = 0; ik < numElem - 1; ++ik){
			for(jk = 0; jk < numElem - 1; ++jk){
				fprintf(fp, "%f %f %f\n", -72 + ik*dx, -72 + jk*dx, pot_cur[ik][jk]);
			}
		}
	
	
	fclose(fp);
	zageszczenieSiatki(pot_cur, k[0]);


	for (i = 1; i <= 5; ++i){
		printf("hejka\n");
		zageszczenieSiatki(pot_cur, k[i]); // WTF NIE TU POWINNO STAC

		char name[10] = {'\n'};
		snprintf(name, 5, "%d", k[i]);
		strcat(name, ".map");
		FILE* fp = fopen(name, "w");

		newPotencjal2(omega, gestosc, pot_pre, pot_cur, k[i]); // wzor (3)
		
		
		integral_pre = newIntegral2(pot_cur, gestosc, k[i]);
		// printf("%f %f\n", pot_pre[10][10], pot_cur[10][10]);
		newPotencjal2(omega, gestosc, pot_pre, pot_cur, k[i]);
		
		// printARRR(pot_pre);

		integral_cur = newIntegral2(pot_cur, gestosc, k[i]);
		newPotencjal2(omega, gestosc, pot_pre, pot_cur, k[i]);
		it = 2;
		printf("%d %f %f\n", it, integral_pre, integral_cur);
		while (fabs(integral_cur - integral_pre) > eps){
			integral_pre = integral_cur;
			integral_cur = newIntegral2(pot_cur, gestosc, k[i]);
			newPotencjal2(omega, gestosc, pot_pre, pot_cur, k[i]);

			++it;

			 // fprintf(fp, "%d %f\n", it, integral_cur);
		}
		
		for(ik = 0; ik < numElem - 1; ++ik){
			for(jk = 0; jk < numElem - 1; ++jk){
				fprintf(fp, "%f %f %f\n", -72 + ik*dx, -72 + jk*dx, pot_cur[ik][jk]);
			}
		}


		printf("%d \n", it);

		fclose(fp);
	}
}


int main(){
	zad1();
	zad2(); // nie dokonczone. uprzejmie prosze sprawdzic funkcje zageszczenieSiatki(), jesli jest taka mozliwosc. program dziala dlugo z zadaniem 2.
}