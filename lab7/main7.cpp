#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265358979323846
const int N = 101;
const double tol = 1e-8;
const double dxy = 1.0;
const double ymin = 0.0;
const double ymax = 0.9;



inline double sqr(const double& a){
	return a*a;
}

inline double cube(const double& a){
	return a*a*a;
}

void copyarr(double lhs[101][51], double rhs[101][51]){
	for(int i = 0; i <= 100; ++i)
				for (int j = 0; j <= 50; ++j)
					lhs[i][j] = rhs[i][j];
}


void printFile(FILE* name, const double& dx, double u[101], double u_analit[101]){
	for(int i = 0; i < 101; ++i){
			fprintf(name, "%f \t %f \t %f \n", i*dx, u[i], u_analit[i]);
			
	}
	fprintf(name, "\n \n");
}

void printarr(int lhs[101][51]){
	for(int i = 0; i <= 300; ++i){
		for (int j = 0; j <= 90; ++j)
			printf("%d ", lhs[i][j]);
		printf("\n");
	}
}


void initCondit(const double& dx, const double& dt, double u[101], double v[101], double a[101]){

	for (int i = 0; i <= 100; ++i){
		u[i] = sin(PI * i*dx) - 0.5 * sin(2*PI * i*dx);
		v[i] = 0;
		a[i] = -sqr(PI) * sin(PI * i*dx) + 2*sqr(PI) * sin(2*PI * i*dx);
	}
}

void calcVUA(const double& dx, const double& t, const double& dt, double u[101], double v[101], double v_mid[101], double a[101]){
	u[0] = u[100] = 0.0;
	for (int i = 1; i < 100; ++i){                  // pomijamy kraje (sztywnosc)
		v_mid[i] = v[i] + 0.5 * dt * a[i]; // 12
	}
	for (int i = 1; i < 100; ++i){         
		u[i] += dt * v_mid[i];         // 13
	}                    
	for (int i = 1; i < 100; ++i){
		a[i] = (u[i+1] + u[i-1] - 2*u[i]) / sqr(dx);// 15
	}
	for (int i = 1; i < 100; ++i){
		v[i] = v_mid[i] + 0.5 * dt * a[i];          // 14
	}	
}

void uAnalit(const double& dx, const double& t, double u_analit[101]){
	u_analit[0] = u_analit[100] = 0.0;
	for (int i = 1; i < 100; ++i){
		u_analit[i] = cos(PI*t) * sin(PI*i*dx) - 0.5 * cos(2*PI*t) * sin(2*PI*i*dx);
	}
}

void zad1(){
	FILE* fp1;
	FILE* fp2;
	FILE* fp3;
	FILE* fp4;
	FILE* fp5;
	FILE* fp6;
	FILE* fp_1;
	fp1 = fopen("1.dat", "w");
	// fp2 = fopen("1_1.map", "w");
	// fp3 = fopen("1_2.map", "w");
	// fp4 = fopen("1_3.map", "w");
	// fp5 = fopen("1_4.map", "w");
	// fp6 = fopen("1_5.map", "w");
	// fp_1 = fopen("1.dat", "w");


	double dt = 1.0 / 200.0;
	double dx = 1.0 / 100.0;
	double u[101];
	double v[101];
	double v_mid[101];
	double a[101];
	double u_analit[101];
	int f1, f2, f3, f4, f5, f6;
	f1 = f2 = f3 = f4 = f5 = f6 = 0;
	int it = 0;
	initCondit(dx, dt, u, v, a);

	for (double t = 0.0; t <= 2.0; t+=dt){
		
		calcVUA(dx, t, dt, u, v, v_mid, a);
		uAnalit(dx, t, u_analit);
		
		if ( !(it % 50) ) {
			printFile(fp1, dx, u, u_analit);
			printf("%f \n", t);
		}
		++it;
		
		
	}




	// fclose(fp_1);
	fclose(fp1);
// 	fclose(fp2);
// 	fclose(fp3);
// 	fclose(fp4);
// 	fclose(fp5);
// 	fclose(fp6);
}

void printFile2(FILE* name, const double& dx, const double& t, double u[101]){
	for(int i = 0; i < 101; ++i){
			fprintf(name, "%f \t %f \t %f \n", t, i*dx, u[i]);
			
	}
	fprintf(name, "\n");
}

void initCondit2(const double& dx, const double& dt, double u[101], double v[101], double a[101]){
	for (int i = 0; i < 101; ++i){
		u[i] = exp(-100.0 * sqr(i*dx - 0.5));
		v[i] = 0;
		a[i] = -sqr(PI) * sin(PI * i*dx) + 2*sqr(PI) * sin(2*PI * i*dx);
	}
}


void calcVUA2b(const double& dx, const double& t, const double& dt, double u[101], double v[101], double v_mid[101], double a[101]){
	
	for (int i = 1; i < 100; ++i){                  // pomijamy kraje (sztywnosc)
		v_mid[i] = v[i] + 0.5 * dt * a[i]; // 12
	}
	for (int i = 1; i < 100; ++i){         
		u[i] += dt * v_mid[i];         // 13
	}    
	u[0] = u[1];
	u[100] = u[99];                
	for (int i = 1; i < 100; ++i){
		a[i] = (u[i+1] + u[i-1] - 2*u[i]) / sqr(dx);// 15
	}
	for (int i = 1; i < 100; ++i){
		v[i] = v_mid[i] + 0.5 * dt * a[i];          // 14
	}
	
	
}

void initRho2c(double rho[101], const double& rho0, const double& x0, const double& dx){
	for (int i = 1; i < 100; ++i){
		if (i*dx < x0) 
			rho[i] = 1.0;
		else
			rho[i] = rho0;
	}
}

void calcVUA2c(const double& dx, const double& t, const double& dt, double u[101], double v[101], double v_mid[101], double a[101],  double rho[101]){
	u[0] = u[100] = 0.0;
	for (int i = 1; i < 100; ++i){                  // pomijamy kraje (sztywnosc)
		v_mid[i] = v[i] + 0.5 * dt * a[i]; // 12
	}
	for (int i = 1; i < 100; ++i){         
		u[i] += dt * v_mid[i];         // 13
	}                    
	for (int i = 1; i < 100; ++i){
		a[i] = (u[i+1] + u[i-1] - 2*u[i]) / (sqr(dx) * rho[i]);// 15
	}
	for (int i = 1; i < 100; ++i){
		v[i] = v_mid[i] + 0.5 * dt * a[i];          // 14
	}	
}

void zad2(){
	double dt = 1.0 / 200.0;
	double dx = 1.0 / 100.0;
	double u[101];
	double v[101];
	double v_mid[101];
	double a[101];
	int it = 0;
	FILE* fp1;
	fp1 = fopen("2a.dat", "w");

	initCondit2(dx, dt, u, v, a);
	/*2a*/ 
	for (double t = 0.0; t <= 4.0; t+=dt){
		calcVUA(dx, t, dt, u, v, v_mid, a);
		printFile2(fp1, dx, t, u);
		++it;
	}

	fclose(fp1);
	/*2b*/
	it =0;
	fp1 = fopen("2b.dat", "w");
	for (double t = 0.0; t <= 4.0; t+=dt){
		calcVUA2b(dx, t, dt, u, v, v_mid, a);
		printFile2(fp1, dx, t, u);
		++it;
	}
	fclose(fp1);

	/*2c*/
	double x0 = 0.75;
	double rho0 = 3.0;
	double rho[101];
	initRho2c(rho, rho0, x0, dx);
	it =0;
	fp1 = fopen("2c.dat", "w");
	for (double t = 0.0; t <= 4.0; t+=dt){
		calcVUA2c(dx, t, dt, u, v, v_mid, a, rho);
		printFile2(fp1, dx, t, u);
		++it;
	}
	fclose(fp1);

}

void calcVUA3(const double& dx, const double& t, const double& dt, double u[101], double v[101], double v_mid[101], double a[101], const double& beta, double u_pre[101]){
	u[0] = u[100] = 0.0;

	for (int i = 1; i < 100; ++i){                  // pomijamy kraje (sztywnosc)
		v_mid[i] = v[i] + 0.5 * dt * a[i]; // 12
	}
	for (int i = 1; i < 100; ++i){ 
		u_pre[i] = u[i];        
		u[i] += dt * v_mid[i];         // 13
	}                    
	for (int i = 1; i < 100; ++i){
		a[i] = (u[i+1] + u[i-1] - 2*u[i]) / (sqr(dx)) - 2*beta * (u[i] - u_pre[i]) / dt;// 15
	}
	for (int i = 1; i < 100; ++i){
		v[i] = v_mid[i] + 0.5 * dt * a[i];          // 14
	}	
}

void zad3(){
	double dt = 1.0 / 200.0;
	double dx = 1.0 / 101.0;
	double u[101];
	double u_pre[101];
	double v[101];
	double v_mid[101];
	double a[101];
	int it = 0;
	FILE* fp1;
	double beta = 0.2;
 	/*3a*/
	initCondit2(dx, dt, u, v, a);
	fp1 = fopen("3a.dat", "w");
	for (double t = 0.0; t <= 4.0; t+=dt){
		calcVUA3(dx, t, dt, u, v, v_mid, a, beta, u_pre);
		printFile2(fp1, dx, t, u);
		++it;
	}
	fclose(fp1);

	/*3b*/
	beta = 1.0;
	initCondit2(dx, dt, u, v, a);
	fp1 = fopen("3b.dat", "w");
	for (double t = 0.0; t <= 4.0; t+=dt){
		calcVUA3(dx, t, dt, u, v, v_mid, a, beta, u_pre);
		printFile2(fp1, dx, t, u);
		++it;
	}
	fclose(fp1);

	/*3c*/
	beta = 3.0;
	initCondit2(dx, dt, u, v, a);
	fp1 = fopen("3c.dat", "w");
	for (double t = 0.0; t <= 4.0; t+=dt){
		calcVUA3(dx, t, dt, u, v, v_mid, a, beta, u_pre);
		printFile2(fp1, dx, t, u);
		++it;
	}
	fclose(fp1);
}

void calcVUA4(const double& omega, const double& dx, const double& t, const double& dt, double u[101], double v[101], double v_mid[101], double a[101], const double& beta, double u_pre[101]){
	u[0] = u[100] = 0.0;
	double x0 = 0.5;
	
	for (int i = 1; i < 100; ++i){                  // pomijamy kraje (sztywnosc)
		v_mid[i] = v[i] + 0.5 * dt * a[i]; // 12
	}
	for (int i = 1; i < 100; ++i){ 
		u_pre[i] = u[i];        
		u[i] += dt * v_mid[i];         // 13
	}                    
	for (int i = 1; i < 100; ++i){
		a[i] = (u[i+1] + u[i-1] - 2*u[i]) / (sqr(dx)) - 2*beta * (u[i] - u_pre[i]) / dt + 
																							cos(omega * t) * exp(-1e5 * sqr(i*dx - x0));// 15
	}
	for (int i = 1; i < 100; ++i){
		v[i] = v_mid[i] + 0.5 * dt * a[i];          // 14
	}	
}

void initCondit4(const double& omega, const double& dx, const double& dt, double u[101], double v[101], double a[101]){
	double x0 = 0.5;
	for (int i = 0; i < 101; ++i){
		u[i] = 0.0;
		v[i] = 0.0;
		a[i] = 0.0 + cos(omega * 0.0) * exp(-1e5 * sqr(i*dx - x0));
	}
}

void zad4(){
	double omega = PI * 0.5;
	double dt = 1.0 / 200.0;
	double dx = 1.0 / 100.0;
	double u[101];
	double u_pre[101];
	double v[101];
	double v_mid[101];
	double a[101];
	int it = 0;
	FILE* fp1;
	double beta = 1.0;
	initCondit4(omega, dx, dt, u, v, a);
	fp1 = fopen("4.dat", "w");
	for (double t = 0.0; t <= 10.0; t+=dt){
		calcVUA4(omega, dx, t, dt, u, v, v_mid, a, beta, u_pre);
		printFile2(fp1, dx, t, u);
		++it;
	}
	fclose(fp1);
}


double calcEnergy(double u[101], double v[101], const double& dx ){
	double energy = 0.0;
	double intv = 0.0;
	double intu = 0.0;
	int i = 0;
	for (double x = 0.0; x <= 1.0; x+=dx, ++i){
		intv += sqr(v[i]) * dx;
	}
	i = 0;
	for (double x = 0.0; x <= 1.0; x+=dx, ++i){
		intu += sqr((u[i] - u[i-1]) / dx) * dx;
	}

	energy = 0.5 * intv + 0.5 * intu;

	return energy;
}

void zad5(){
		double dt = 1.0 / 200.0;
	double dx = 1.0 / 100.0;
	double domega = 0.01*PI;
	double u[101];
	double u_pre[101];
	double v[101];
	double v_mid[101];
	double a[101];
	int it = 0;
	FILE* fp1;
	double beta = 1.0;
	
	fp1 = fopen("5.dat", "w");
	double energy;
	for (double omega = 0.0; omega <= 10.0*PI; omega+=domega){
		initCondit4(omega, dx, dt, u, v, a);
		energy = 0.0;

		for (double t = 0.0; t <= 20.0; t+=dt){
			calcVUA4(omega, dx, t, dt, u, v, v_mid, a, beta, u_pre);
			if (t >= 16.0)
				energy += calcEnergy(u, v, dx) * dt;
			// printFile2(fp1, dx, t, u, );
		}
		energy  *= 0.25;
		fprintf(fp1, "%e \t %e \n", omega, energy);
	}

	fclose(fp1);
}
int main(){
	zad1();
	zad2();
	zad3();
	zad4();
	zad5();

}